// console.log("Hello World")

const express = require("express")
const app = express()
const PORT = process.env.PORT || 4000
const cors = require("cors")
const mongoose = require("mongoose")
//"mongodb+srv://Doms1234:Doms1234@b49-ecommerce-82cxa.mongodb.net/BANDArito?retryWrites=true&w=majority"
//"mongodb://localhost/BANDArito"
mongoose.connect("mongodb+srv://Doms1234:Doms1234@b49-ecommerce-82cxa.mongodb.net/BANDArito?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

const db = mongoose.connection
db.once("open", () => console.log("We are connected to MongoDB"))

app.use(cors())
app.use(express.json())
app.use(express.static('public')) //to access on browser the files

//implement URL and access routes
app.use("/categories", require("./routes/categories"))
app.use("/genres", require("./routes/genres"))
app.use("/statuses", require("./routes/statuses"))
app.use("/rooms", require("./routes/rooms"))
app.use("/users", require("./routes/users"))
app.use("/transactions", require("./routes/transactions"))

app.listen(PORT, () => console.log("Server is running in port " + PORT))