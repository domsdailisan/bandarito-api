const jwt = require("jsonwebtoken")
const User = require("./models/User")

const isAdmin = (req, res, next) => {
	let token = req.headers['x-auth-token']
	if(!token) return res.json({message: "Not logged in"})
	jwt.verify(token, 'BANDArito', (err, decoded) => {
		if(decoded.isAdmin === false) return res.status(401).json({
			status: 401,
			message:"Unauthorized"
		})
		next()
	})
}

module.exports = isAdmin