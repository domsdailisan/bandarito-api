const mongoose = require("mongoose")
const Schema = mongoose.Schema

const RoomSchema = new Schema({
	name: {
		type: String,
		required: [true, "Room name is required"]
	},
	description: {
		type: String,
		required: [true, "Desciption is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	genre: {
		type: String,
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},
	image: {
		type: String,
		required: [true, "Product image is required"]
	}
})

module.exports = mongoose.model("Room", RoomSchema)