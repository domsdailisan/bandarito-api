const mongoose = require("mongoose")
const Schema = mongoose.Schema
const User = require("./Status")

const TransactionSchema = new Schema({
	userId: {
		type: String,
		required: true
	},

	statusId: {
		type: String,
		default: "5e9d2e50e6e19814785be47d",
		required: true
	},

	dateCreated: {
		type: Date,
		default: Date.now()
	},

	paymentMode: {
		type: String,
		required: true
	},

	total: {
		type: Number,
		required: true
	},

	rooms: [
		{
			productId: String,
			name: String,
			price: Number,
			quantity: Number,
			subtotal: Number
		}
	],

	sched: {
		type: String,
		required: true
	}
})

module.exports = mongoose.model("Transaction", TransactionSchema)