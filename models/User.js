const mongoose = require("mongoose")
const Schema = mongoose.Schema

const UserSchema = new Schema ({
	username: {
		type: String,
		required: true
	},

	bandName: {
		type: String,
	},
	password: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	contact: {
		type: Number,
		required: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model("User", UserSchema)