const Category = require("../models/Category")
const express = require("express")
const router = express.Router()

//view all categories
router.get("/", (req, res) => {
	// console.log("You've reached view all categories")
	Category.find({}, (err, categories) =>{
		return res.json(categories)
	})
})

//add a category
router.post("/add-category", (req, res) => {
	// console.log("You've reached add new category")
	let category = new Category()
	category.name = req.body.name
	category.save()
	return res.json({
		category,
		message: "Added new category",
		status: 200
	})
})

//Update a category
router.put("/:id", (req, res)=> {
	let updatedCategory = {...req.body}
	Category.findOneAndUpdate(
		{_id: req.params.id},
		updatedCategory,
		{new: true},
		(err, updated) => {
			return res.json({
				updated,
				message: "Category updated successfully",
				status: 200
			})
		}
	)
})


module.exports = router