const Genre = require("../models/Genre")
const express = require("express")
const router = express.Router()

//view all genres
router.get("/", (req, res) => {
	// console.log("You've reached view all categories")
	Genre.find({}, (err, genres) =>{
		return res.json(genres)
	})
})

//add a genre
router.post("/add-genre", (req, res) => {
	// console.log("You've reached add new genre")
	let genre = new Genre()
	genre.name = req.body.name
	genre.save()
	return res.json({
		genre,
		message: "Added new genre",
		status: 200
	})
})

//Update a genre
router.put("/:id", (req, res)=> {
	let updateGenre = {...req.body}
	Genre.findOneAndUpdate(
		{_id: req.params.id},
		updateGenre,
		{new: true},
		(err, updated) => {
			return res.json({
				updated,
				message: "Genre updated successfully",
				status: 200
			})
		}
	)
})


module.exports = router