const Room = require("../models/Room")
const express = require("express")
const router = express.Router()
const Product = require("../models/Room")
const isAdmin = require("../auth")
const multer = require("multer")

//view all
router.get("/", (req, res) => {
	// console.log("You've reached LANDING PAGE")
	Room.find({}, (err, rooms) =>{
		return res.json(rooms)
	})
})

//add room
let storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, "public/rooms")
	},
	filename: (req, file, cb) => {
		cb(null, new Date().getTime() + "-" + file.originalname)
	}
})

let upload = multer({storage: storage})

router.post("/", upload.single('image'), isAdmin, (req, res) => {
	let room = new Room()
	room.name = req.body.name
	room.description = req.body.description
	room.price = req.body.price
	room.genre = req.body.genre
	room.category = req.body.category
	room.image = "/rooms/"+req.file.filename
	room.save()
	return res.json({
		room, 
		message: "Room added successfully",
		status: 200
	})
})

//view single room
router.get('/:id', (req, res) => {
	Room.findOne({_id: req.params.id}, (err, room) => {
		return res.json(room)
	})
})

//delete single room
router.delete('/:id', isAdmin, (req, res) => {
	Room.findOneAndDelete({_id: req.params.id}, (err, room) => {
		return res.json({
			room, 
			message: "Room deleted successfully", 
			status: 200
		})
	})
})

//edit room details
router.put("/:id", upload.single('image'), isAdmin, (req, res) => {
	let updatedRoom = {...req.body}
	if(req.file) {
		updatedRoom = {...req.body, image: "/rooms/"+req.file.filename}
	}
	Room.findOneAndUpdate(
		{_id: req.params.id},
		updatedRoom,
		{new: true},
		(err, updated) => {
			return res.json({
				updated, 
				message: "Room details updated successfully", 
				status: 200
			})
		}
	)
})

module.exports = router