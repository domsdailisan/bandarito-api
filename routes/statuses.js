const Status = require("../models/Status")
const express = require("express")
const router = express.Router()

//view all
router.get("/", (req, res) => {
	Status.find({}, (err, statuses) =>{
		return res.json(statuses)
	})
})

//add
router.post("/add-status", (req, res) => {
	// console.log("You've reached add new status")
	let status = new Status()
	status.name = req.body.name
	status.save()
	return res.json({
		status,
		message: "Added new status"
	})
})

module.exports = router