const express = require("express")
const router = express.Router()
const Product = require("../models/Room")
const Transaction = require("../models/Transaction")
const User = require("../models/User")
const isAdmin = require("../auth")
const jwt = require("jsonwebtoken")
const stripe = require("stripe")('sk_test_xHo77hLJxKTAAyRa3smmgZ4900KdWnaS6X')


//Add transaction POST http://localhost:4000/transactions
router.post("/", (req, res) => {
	// console.log(req.body)
	let token = req.headers['x-auth-token']
	if(!token) return res.status(401).json({status: 401, message: "You are not logged in"})

	let decoded = jwt.verify(token, 'BANDArito')
	// console.log(req.body)
	if(decoded) {
		let transaction = new Transaction()
		transaction.userId = decoded._id
		transaction.paymentMode = "COD"
		transaction.total = req.body.total
		transaction.rooms = req.body.orders
		transaction.sched = req.body.sched
		console.log(transaction)
		transaction.save()
		return res.json({status: 200, message: "Transaction successful"})

	}
})

//get all transaction for admin
router.get("/", isAdmin, (req, res) => {
	Transaction.find({}, (err, transactions) => {
		return res.json(transactions)
	})
})

//admin status update
router.put("/:id", isAdmin, (req, res) => {
	Transaction.findOne({_id: req.params.id}, (err, transaction) => {
		transaction.statusId = req.body.statusId
		transaction.save()
		return res.json({status: 200, message: "Status changed"})
	}) 
})

//get all transactions for user
//localhost:4000/transactions/userId
router.get("/:userId", (req, res) => {
	let token = req.headers["x-auth-token"]
	// console.log(token)
	if(!token) return res.json({status: 401, message: "You are not logged in"})
	
	Transaction.find({userId: req.params.userId})
	.then(transaction => res.json(transaction))
})

router.post("/stripe", (req, res) => {
	// console.log(req.body)
	let token = req.headers["x-auth-token"]
	if(!token) return res.status(401).json({status: 401, message: "You are not logged in"})

	let decoded = jwt.verify(token, 'BANDArito')

	if(decoded) {
		let transaction = new Transaction()
		console.log(req.body)
		transaction.userId = decoded._id
		transaction.paymentMode = "Stripe"
		transaction.total = req.body.amount
		transaction.rooms = req.body.cartItems
		transaction.sched = req.body.sched
		console.log(transaction)

		let body = {
			source: req.body.token.id,
			amount: req.body.amount,
			email: req.body.email,
			currency: "PHP"
		}

		stripe.charges.create(body, (err, result) => {
			if(result) {
				console.log(result)
				transaction.save()
				return res.json({status: 200, message: "Transaction successful"})
			}else {
				return res.send(err)
			}
		}) 
	}
})

module.exports = router