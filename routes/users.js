const User = require("../models/User")
const express = require("express")
const router = express.Router()
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")

//Register account
router.post("/register", (req, res) => {
	//Username must be greater than 8 chars
	if(req.body.username.length < 8) return res.status(400).json({
		status: 400,
		message: "Username must be greater than 8 characters"
	})
	//Password must be greater than 8 chars
	if(req.body.password.length < 8) return res.status(400).json({
		status: 400,
		message: "Password must be greater than 8 characters"
	})
	if(req.body.password2.length < 8) return res.status(400).json({
		status: 400,
		message: "Password2 must be greater than 8 characters"
	})
	//Passwords should match
	if(req.body.password != req.body.password2) return res.status(400).json({
		status: 400,
		message: "Password does not match"
	})
	//Check if username already exists
	User.findOne({username: req.body.username}, (err, user) => {
		if(user) return res.status(400).json({
			status: 400,
			message: "Username already exists"
		})
		bcrypt.hash(req.body.password, 10, (err, hashedPassword) => {
			const user = new User()
			user.username = req.body.username
			user.bandName = req.body.bandName
			user.password = hashedPassword
			user.email = req.body.email
			user.contact = req.body.contact
			user.save()
			return res.json({
				status:200,
				message: "Registration Successful!"
			})
		})
	})
})

//Login
router.post("/login", (req, res) => {
	User.findOne({username: req.body.username}, (err, user) => {
		if(err || !user) return res.status(400).json({
			status: 400,
			message: "No user found or Incorrect username"
		})
		bcrypt.compare(req.body.password, user.password, (err, result) => {
			if(!result){
				return res.status(401).json({
					auth: false,
					message: "Invalid credentials or Incorrect password",
					token: null
				})
			}else {
				user = user.toObject() // need to add(.toObject) this since we cant apply "delete" on data from mongoose
				delete user.password
				let token = jwt.sign(user, 'BANDArito', {expiresIn: '5h'})
				return res.status(200).json({
					auth: true,
					message: "Logged in Successfully",
					user,
					token
				})
			}
		})
	})
})


module.exports = router